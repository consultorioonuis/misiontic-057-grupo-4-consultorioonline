-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-09-2022 a las 04:44:44
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prueba`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cita`
--

CREATE TABLE `cita` (
  `idcita` int(11) NOT NULL,
  `cedulap` int(11) NOT NULL,
  `ids` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `consultorio` varchar(45) NOT NULL,
  `duracion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cita`
--

INSERT INTO `cita` (`idcita`, `cedulap`, `ids`, `fecha`, `hora`, `consultorio`, `duracion`) VALUES
(5, 13746778, 1, '2022-09-15', '16:00:00', '705', 25),
(7, 11234786, 3, '2022-09-16', '07:00:00', '203', 20),
(8, 17467784, 2, '2022-09-23', '11:00:00', '507', 30);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permiso`
--

CREATE TABLE `permiso` (
  `idpermiso` int(11) NOT NULL,
  `nombrepermiso` varchar(45) NOT NULL,
  `permisocol` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `permiso`
--

INSERT INTO `permiso` (`idpermiso`, `nombrepermiso`, `permisocol`) VALUES
(1, 'Administrador', '1'),
(2, 'Medico', '2'),
(3, 'Usuario', '3'),
(6, 'Modificar', '2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisosegunrol`
--

CREATE TABLE `permisosegunrol` (
  `idpsr` int(11) NOT NULL,
  `idp` int(11) NOT NULL,
  `idr` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `permisosegunrol`
--

INSERT INTO `permisosegunrol` (`idpsr`, `idp`, `idr`) VALUES
(1, 1, 1),
(2, 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `cedula` int(30) NOT NULL,
  `tipodocumento` varchar(45) NOT NULL,
  `contrasena` varchar(45) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `fechanacimiento` varchar(45) NOT NULL,
  `sexo` varchar(45) NOT NULL,
  `telefono` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `direccion` varchar(45) NOT NULL,
  `ciudad` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`cedula`, `tipodocumento`, `contrasena`, `nombre`, `apellido`, `fechanacimiento`, `sexo`, `telefono`, `email`, `direccion`, `ciudad`) VALUES
(11234786, 'Cedula extranjeria', '43654', 'Gabriela', 'Manrique', '13 Jun 1987', 'Femenino', '3228456732', 'gaby@gmail.com', 'Av. principal', 'Ibague'),
(13746778, 'Cedula Ciudadania', '12345', 'Robinson', 'Martinez', '16 Dic 1980', 'Masculino', '3204322195', 'rodmart2@hotmail.com', 'Cra 12', 'Bucaramanga'),
(17467784, 'Cedula Ciudadania', '123456', 'Pedro', 'Quiroz', '12 Ene 1991', 'Masculino', '3224462138', 'pedro.quiroz@hotmail.com', 'Calle 9', 'Bogota'),
(1005534782, 'Cedula ciudadania', '9227', 'Juan', 'Perez', '03 Sep 1978', 'Masculino', '3183236785', 'juanp@gmail.com', 'Av. Colon', 'Cali');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `idrol` int(11) NOT NULL,
  `nombrerol` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`idrol`, `nombrerol`) VALUES
(1, 'Administrador'),
(2, 'Medico'),
(3, 'Usuario'),
(4, 'Enfermero Auxiliar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rolpersona`
--

CREATE TABLE `rolpersona` (
  `idrolpersona` int(11) NOT NULL,
  `cedulapersona` int(11) NOT NULL,
  `rolpersona` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `rolpersona`
--

INSERT INTO `rolpersona` (`idrolpersona`, `cedulapersona`, `rolpersona`) VALUES
(1, 13746778, 1),
(2, 17467784, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--

CREATE TABLE `servicio` (
  `idservicio` int(11) NOT NULL,
  `nombreservicio` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `servicio`
--

INSERT INTO `servicio` (`idservicio`, `nombreservicio`) VALUES
(1, 'Medicina general'),
(2, 'Odontologia'),
(3, 'Pediatria');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cita`
--
ALTER TABLE `cita`
  ADD PRIMARY KEY (`idcita`),
  ADD UNIQUE KEY `cedulap` (`cedulap`,`ids`),
  ADD KEY `citaservicio` (`ids`);

--
-- Indices de la tabla `permiso`
--
ALTER TABLE `permiso`
  ADD PRIMARY KEY (`idpermiso`);

--
-- Indices de la tabla `permisosegunrol`
--
ALTER TABLE `permisosegunrol`
  ADD PRIMARY KEY (`idpsr`),
  ADD UNIQUE KEY `idp` (`idp`),
  ADD UNIQUE KEY `idr` (`idr`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`cedula`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`idrol`);

--
-- Indices de la tabla `rolpersona`
--
ALTER TABLE `rolpersona`
  ADD PRIMARY KEY (`idrolpersona`),
  ADD UNIQUE KEY `cedulapersona` (`cedulapersona`),
  ADD UNIQUE KEY `rolpersona` (`rolpersona`);

--
-- Indices de la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD PRIMARY KEY (`idservicio`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cita`
--
ALTER TABLE `cita`
  MODIFY `idcita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `permiso`
--
ALTER TABLE `permiso`
  MODIFY `idpermiso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `idrol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `servicio`
--
ALTER TABLE `servicio`
  MODIFY `idservicio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cita`
--
ALTER TABLE `cita`
  ADD CONSTRAINT `citapersona` FOREIGN KEY (`cedulap`) REFERENCES `persona` (`cedula`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `citaservicio` FOREIGN KEY (`ids`) REFERENCES `servicio` (`idservicio`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `permisosegunrol`
--
ALTER TABLE `permisosegunrol`
  ADD CONSTRAINT `permisodelrol` FOREIGN KEY (`idp`) REFERENCES `permiso` (`idpermiso`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rolparapermiso` FOREIGN KEY (`idr`) REFERENCES `rol` (`idrol`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `rolpersona`
--
ALTER TABLE `rolpersona`
  ADD CONSTRAINT `rolasignado` FOREIGN KEY (`cedulapersona`) REFERENCES `persona` (`cedula`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tiporol` FOREIGN KEY (`rolpersona`) REFERENCES `rol` (`idrol`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
